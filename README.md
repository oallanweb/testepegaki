# Teste Pegaki
Esse são os passos que deve ser seguidos para testar o projeto.

#### Backend
* Entre na pasta ` backend `
* instale as dependencias ` npm install `
* rode no modo debug ` npm run dev `
* Ou rode no modo start ` npm start `

    ##### Testes
    * Login 
    ```
        // url http://localhost:3000/login
        // body
        {
            "email" : "allan@teste.com",
            "password" : "123"
        }
        
        /* Retono */
        
        { "token" : "[token de retono]" } 
    ```
    
    * Busca por CEP 
    ```
        // url http://localhost:3000/places/{cep}
        
        /* Retono */
        
        {
    "results": [
        {
            "id": "1326",
            "nome_fantasia": "MULHERES HABILITADAS - VILA MARIANA",
            "endereco": "RUA LOEFGREN",
            "numero": "1312",
            "complemento": "Shopping Metro Santa Cruz ",
            "bairro": "VILA CLEMENTINO",
            "cep": "04040001",
            "cidade": "São Paulo",
            "estado": "SP",
            "telefone": "1138056360",
            "latitude": "-23.59932000",
            "longitude": "-46.64654400",
            "foto": "https://imagemcliente.s3.sa-east-1.amazonaws.com/17d19a538f70c5fb143cf8255ddbb330.png",
            "distancia": "0,73",
            "horario_funcionamento": {
                "segunda": "07:00 - 20:00",
                "terca": "07:00 - 20:00",
                "quarta": "07:00 - 20:00",
                "quinta": "07:00 - 20:00",
                "sexta": "07:00 - 20:00",
                "sabado": "07:00 - 17:00",
                "almoco": " - "
            },
            "features": [
                "Horário estendido",
                "Próximo Metrô",
                "Shopping",
                "Mercado"
            ]
        },
        {
            "id": "1943",
            "nome_fantasia": "LEDGOLD",
            "endereco": "Avenida Indianópolis",
            "numero": "1860",
            "complemento": "",
            "bairro": "Indianópolis",
            "cep": "04062002",
            "cidade": "São Paulo",
            "estado": "SP",
            "telefone": "1155940339",
            "latitude": "-23.61457700",
            "longitude": "-46.65013800",
            "foto": "https://imagemcliente.s3.sa-east-1.amazonaws.com/fe4712ed717a23b7f0966a0c348573c6.png",
            "distancia": "1,06",
            "horario_funcionamento": {
                "segunda": "09:00 - 18:00",
                "terca": "09:00 - 18:00",
                "quarta": "09:00 - 18:00",
                "quinta": "09:00 - 18:00",
                "sexta": "09:00 - 18:00",
                "sabado": "09:00 - 15:00",
                "almoco": " - "
            },
            "features": [
                "Estacionamento",
                "Próximo Metrô"
            ]
        },
        {
            "id": "1979",
            "nome_fantasia": "BUNKER ESPAÇO INTEGRADO",
            "endereco": "Rua Caramuru",
            "numero": "1042",
            "complemento": "",
            "bairro": "Saúde",
            "cep": "04138002",
            "cidade": "São Paulo",
            "estado": "SP",
            "telefone": "11982117776",
            "latitude": "-23.61315700",
            "longitude": "-46.63549500",
            "foto": "https://imagemcliente.s3.sa-east-1.amazonaws.com/7bc4ac8fa3d48521a44114b8c57ddb91.png",
            "distancia": "1,34",
            "horario_funcionamento": {
                "segunda": "09:00 - 19:00",
                "terca": "09:00 - 19:00",
                "quarta": "09:00 - 19:00",
                "quinta": "09:00 - 19:00",
                "sexta": "09:00 - 19:00",
                "sabado": "09:00 - 17:00",
                "almoco": " - "
            },
            "features": [
                "Horário estendido",
                "Próximo Metrô"
            ]
        },
        {
            "id": "147",
            "nome_fantasia": "Vaz Cafe",
            "endereco": "Alameda dos Maracatins",
            "numero": "521",
            "complemento": "",
            "bairro": "Indianópolis",
            "cep": "04089011",
            "cidade": "São Paulo",
            "estado": "SP",
            "telefone": "1129241232",
            "latitude": "-23.60546030",
            "longitude": "-46.65921860",
            "foto": "https://s3-sa-east-1.amazonaws.com/imagemcliente/default.png",
            "distancia": "1,36",
            "horario_funcionamento": {
                "segunda": "08:00 - 17:30",
                "terca": "08:00 - 17:30",
                "quarta": "08:00 - 17:30",
                "quinta": "08:00 - 17:30",
                "sexta": "08:00 - 17:30",
                "sabado": "09:00 - 13:00",
                "almoco": " - "
            },
            "features": []
        },
        {
            "id": "305",
            "nome_fantasia": "Bora Mergulhar",
            "endereco": "Avenida Chibarás",
            "numero": "261",
            "complemento": "",
            "bairro": "Moema",
            "cep": "04076000",
            "cidade": "São Paulo",
            "estado": "SP",
            "telefone": "1143712672",
            "latitude": "-23.60287190",
            "longitude": "-46.65884140",
            "foto": "https://s3-sa-east-1.amazonaws.com/imagemcliente/default.png",
            "distancia": "1,36",
            "horario_funcionamento": {
                "terca": "10:00 - 18:00",
                "quarta": "10:00 - 18:00",
                "quinta": "10:00 - 18:00",
                "sexta": "10:00 - 18:00",
                "sabado": "10:00 - 13:00",
                "almoco": " - "
            },
            "features": []
        }
            
        }
        ]
    ```

    * Busca por CEP retona pontos.XLSX 
    ```
        // url http://localhost:3000/places/{cep}/xlsx
        
        /* Retono */
       pontos.xslx
    ```

