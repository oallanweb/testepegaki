import React from 'react';
import { withRouter } from 'react-router-dom';
import {
  Container,
  WrapperImage,
  WrapperForm,
  Img,
  Form,
  Field,
  Logo,
  Erro,
  Button
} from './styled';
import { Formik } from 'formik';
import { login } from '../../services/validate';
import { rawApi } from '../../services/api';
import svg from '../../assets/world_connection_.svg';
import logo from '../../assets/logo.png';
import swal from 'sweetalert';
import { login as loginAuth } from '../../services/auth';

const Login = ({ history }) => {

  const handlerLogin = async (values, actions) => {
    const res = await rawApi.post('/login', values);
    const { error, token } = res;
    if (error) {
      swal({
        title: 'Erro!',
        text: error,
        icon: 'error',
      });
      return;
    }
    loginAuth(token);
    history.push('/home')
  }

  return (
    <Container>
    <WrapperImage>
      <Img src={svg} />
    </WrapperImage>
    <WrapperForm>
    <Formik
    initialValues={{ email:'', password:'' }}
    validationSchema={login}
    onSubmit={handlerLogin}
    >
    {props => (
      <Form onSubmit={props.handleSubmit}>
      <Logo src={logo}/>
            <Field>
              <label>
                E-mail
              </label>
              <input
                type='email'
                onChange={props.handleChange}
                onBlur={props.handleBlur}
                value={props.values.email}
                name='email'
                id='email'
                placeholder='Seu e-mail'
                />
                {props.errors.email && <Erro>{props.errors.email}</Erro>}
                </Field>  
                <Field>
                <label>
                Senha
                </label>
                <input
                type='password'
                onChange={props.handleChange}
                onBlur={props.handleBlur}
                value={props.values.password}
                name='password'
                id='password'
                placeholder='Sua Senha'
              />
              {props.errors.password && <Erro>{props.errors.password}</Erro>}
            </Field>  
            <Button type='submit'>ENTRAR</Button>
          </Form>
        )}
      </Formik>
    </WrapperForm>
  </Container>
  );
};

export default withRouter(Login);
