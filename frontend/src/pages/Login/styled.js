import styled from 'styled-components';

export const Container = styled.div`
  width: 100%;
  height: 100vh;
  display: flex;
`;

export const Wrapper = styled.div`
  width: 50%;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  @media only screen and (max-width: 760px) {
    width: 100%;
  }
`;

export const WrapperImage = styled.div`
  width: 50%;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  @media only screen and (max-width: 760px) {
    display:none;
  }
`;

export const WrapperForm = styled(Wrapper)`
  background-color: #EFEFEF;
`;

export const Img = styled.img`
  width: 100%;
`;

export const Form = styled.form`
  max-width: 300px;
  display: flex;
  flex-direction: column;
  width: 100%;
  box-sizing: border-box;
  `;

export const Logo = styled.img`
  width: 96%;
  `;

export const Field = styled.div`
  box-sizing: border-box;
  width: 100%;
  margin-top: 20px;
  & > input {
    box-sizing: border-box;

    height: 50px;
    width: 100%;
    padding: 0 15px;
    border-radius: 25px;
    border: none;
    -webkit-box-shadow: 0px 0px 35px -10px rgba(153,153,153,1);
    -moz-box-shadow: 0px 0px 35px -10px rgba(153,153,153,1);
    box-shadow: 0px 0px 35px -10px rgba(153,153,153,1);
    margin: 5px 0;
  }
  & > label {
    padding: 0 15px;
    font-family: sans-serif;
  }
  
  `;
  
  export const Erro = styled.div`
  color: red;
  margin-left: 20px;
  font-family: sans-serif;
`;
  
export const Button = styled.button`
  background: #3AB54B;
  color: #FFFFFF;
  width: 100%;
  height: 50px;
  border: none;
  border-radius: 8px;
  font-weight: 700;
  &:hover{
    background: #318E41;
  }
  margin-top:20px
`;
