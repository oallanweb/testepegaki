import React, { useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import { Container } from './styled';
import GoogleMapReact from 'google-map-react';

const AnyReactComponent = ({ text }) => <div>{text}</div>;

const Home = ({history}) => {
  const [lat, setLat] = useState(0);
  const [lng, setLng] = useState(0);
  
  const onChange = ({coords}) => {

    console.log(lat)
    setLat(coords.latitude)
    setLng(coords.longitude)
  }
  const onError = (error) => {
    return
  };
  useEffect(() => {
    const geo = navigator.geolocation;
    if (!geo) {
      return;
    }
    geo.watchPosition(onChange, onError);
  }, []);

  return (
    <Container> 
      <GoogleMapReact
      bootstrapURLKeys={{ key: 'AIzaSyA1Vm0V7FLRhGGbMnMFRiSP9rtAkw-ndzg'}}
      defaultCenter={{
        lat,
        lng
      }}
      defaultZoom={4}
    >
      <AnyReactComponent
        lat={59.955413}
        lng={30.337844}
        text="My Marker"
      />
    </GoogleMapReact>
    </Container>
  )
}

export default withRouter(Home);