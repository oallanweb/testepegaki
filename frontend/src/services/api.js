import axios from "axios";
import { getToken } from "./auth";

const api = axios.create({
  baseURL: "http://127.0.0.1:3333"
});

let rawApi = axios.create({
  baseURL: "http://127.0.0.1:3333"
});


rawApi.interceptors.response.use(
  res => res?.data,
  error => error?.response?.data 
    || { error: "Ocorreu algum erro a conexão com o servidor!" }
)

export { rawApi };

api.interceptors.request.use(async config => {
  const token = getToken();
  if (token) {
    config.headers['x-access-token'] = `${token}`;
  }
  return config;
});

export default api;