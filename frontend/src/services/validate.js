import * as yup from 'yup';

const defaultMessage = 'Obrigatório';

export const login = yup.object().shape({
  email: yup
      .string()
      .email("E-mail inválido!")
      .required(defaultMessage),
  password: yup
      .string()
      .required(defaultMessage),
});